INSERT INTO `edu_chapter`(`id`, `course_id`, `title`, `sort`, `gmt_create`, `gmt_modified`) VALUES ('1', '1', 'Java语言概述', 0, '2021-09-24 14:12:11', '2021-09-24 14:12:13');
INSERT INTO `edu_chapter`(`id`, `course_id`, `title`, `sort`, `gmt_create`, `gmt_modified`) VALUES ('1443531985455677441', '1443531931495956482', '第一章：什么是Python', 0, '2021-09-30 19:03:40', '2021-09-30 19:03:40');
INSERT INTO `edu_chapter`(`id`, `course_id`, `title`, `sort`, `gmt_create`, `gmt_modified`) VALUES ('1443532316793110529', '1443531931495956482', '第二章：什么是爬虫', 0, '2021-09-30 19:04:59', '2021-09-30 19:04:59');
INSERT INTO `edu_chapter`(`id`, `course_id`, `title`, `sort`, `gmt_create`, `gmt_modified`) VALUES ('2', '1', 'Java基础语法', 0, '2021-09-24 14:12:31', '2021-09-24 14:12:34');
INSERT INTO `edu_chapter`(`id`, `course_id`, `title`, `sort`, `gmt_create`, `gmt_modified`) VALUES ('3', '1', 'Java面向对象', 0, '2021-09-24 14:13:04', '2021-09-24 14:13:07');
INSERT INTO `edu_chapter`(`id`, `course_id`, `title`, `sort`, `gmt_create`, `gmt_modified`) VALUES ('4', '1', 'Java集合', 0, '2021-09-24 14:13:34', '2021-09-24 14:13:39');
INSERT INTO `edu_chapter`(`id`, `course_id`, `title`, `sort`, `gmt_create`, `gmt_modified`) VALUES ('5', '1', 'Java常见类', 0, '2021-09-24 14:14:02', '2021-09-24 14:14:04');
