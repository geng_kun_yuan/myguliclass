# 我的谷粒学院

## 项目介绍

谷粒学院，是一个基于**B2C模式**的在线教育系统，分为前台用户系统和后台运营平台，使用**前后端分离**技术、**微服务架构**模式进行开发。

在**前台用户系统**中，用户可以登录系统，查看系统中的课程和讲师，并且能够对自己喜欢的课程使用微信支付进行购买，并且能够在线播放已经购买的课程。同时用户可以在评论区提出自己的意见，或者是与其他用户交流自己的学习心得。

在**后台运营平台**中，管理员可以对系统中的数据进行管理，例如：用户管理，讲师管理，课程管理，订单管理等。同时管理员可以对用户活跃数，课程受欢迎的程度等数据进行分析，并在此基础上选择合适的营销策略。

## 功能模块描述

| 模块               | 描述          | 功能                                     |
| ------------------ | ------------- | ---------------------------------------- |
| service-cms        | 轮播图模块    | 轮播图展示，轮播图管理                   |
| service-email      | 发送邮件模块  | 邮箱注册，TODO：管理员给用户发送邮件信息 |
| service-msm        | 短信模块      | 发送短信验证码                           |
| service-order      | 订单模块      | 生成订单，订单管理，微信扫码支付         |
| service-oss        | 阿里云OSS模块 | 上传文件到阿里云                         |
| service-ucenter    | 登录注册模块  | 普通登录，微信扫码登录                   |
| service-vod        | 视频点播模块  | 上传视频到阿里云，获取播放地址           |
| service-acl        | 权限管理模块  | 用户的权限管理                           |
| service-edu        | 核心模块      | 讲师管理，课程管理，课程大纲管理         |
| service_statistics | 数据统计模块  | 生成统计数据                             |
| api-getway         | 服务网关模块  | 请求转发，负载均衡                       |

![img](https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20211001184408.png)

## 开发技术

### 后端技术栈

| 技术               | 描述                   | 版本           |
| ------------------ | ---------------------- | -------------- |
| SpringBoot         | SpringBoot框架         | 2.2.1.RELEASE  |
| SpringCloud        | SpringCloud框架        | Hoxton.RELEASE |
| SpringCloudAlibaba | SpringCloudAlibaba框架 | 0.2.2.RELEASE  |
| SpringSecurity     | SpringSecurity框架     | 2.2.1.RELEASE  |
| Redis              | 数据缓存框架           | 6.2.0          |
| MybaticsPlus       |                        |                |
| JWT                | Web令牌                | 0.7.0          |
| EasyExcel          | excel处理工具          | 2.1.1          |
| fastjson           | json解析包             | 1.2.28         |
| swagger-ui         | 接口测试工具           | 2.7.0          |
| hutool             | Java工具类库           | 5.7.13         |

### 前端技术栈

| 技术       | 描述                 | 版本   |
| ---------- | -------------------- | ------ |
| Vue        | 前端开发框架         |        |
| element-ui | 桌面端组件库         | 2.15.6 |
| Jquery     | 快速的JavaScript框架 | 1.11.3 |
| axios      | 网络请求库           | 0.21.4 |
| EChars     | 可视化图表库         | 5.2    |

### 其他技术

| 技术    | 描述                     | 版本   |
| ------- | -------------------| ------ |
| Maven   | Java项目的管理和构建工具 |        |
| MySQL   | 数据库                   | 5.7    |
| Git     | 分布式版本控制系统       | 2.33.0 |
| 阿里云短信服务 | 使用阿里云发送短信 |  |
| 阿里云OSS | 使用阿里云做文件存储 |  |
| 阿里云视频点播 | 使用阿里云做视频播放 |  |
| 微信登录 | 接入微信登录接口 |  |
| 微信支付 | 接入微信支付接口 |  |
| 邮件发送 | 实现发送邮件功能 | |

### 开发工具

| 工具     | 描述                   | 版本    |
| --------| ------------------------| --------|
| IDEA    | 代码编辑器               | 2021.1 |
| VSCode  | 代码编辑器               | 1.60.2 |
| VMware  | 桌面虚拟计算机软件       | 15.0   |
| CentOS  | Linux操作系统          | 7.1    |
| PostMan | 接口测试工具             | 8.8.0  |





## 页面展示

### 前端展示页面

#### 首页数据显示

**首页**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930192415.jpg" alt="首页1" style="zoom:50%;" />

**首页课程信息**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930192020.png" alt="首页2" style="zoom:50%;" />

**首页讲师信息**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191616.png" alt="首页3" style="zoom:50%;" />

#### 讲师展示页面

**讲师目录**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930192707.jpg" alt="讲师目录" style="zoom:50%;" />

**讲师详情**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191642.png" alt="讲师详情1" style="zoom:50%;" />


#### 课程展示页面

**课程目录**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191645.png" alt="课程1" style="zoom:50%;" />

**课程筛选**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191648.png" alt="课程2" style="zoom:50%;" />

**课程详情**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191652.png" alt="课程详情1" style="zoom:50%;" />

**课程详情**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191555.png" alt="课程详情2" style="zoom:50%;" />


#### 购买课程页面

**购买课程**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191636.png" alt="购买课程1" style="zoom:50%;" />

**微信支付**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191639.png" alt="购买课程2" style="zoom:50%;" />

**在线播放**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191624.png" alt="在线播放" style="zoom:50%;" />



#### 登录注册页面

**用户注册**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191629.png" alt="注册" style="zoom:50%;" />

**用户名密码登录**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191633.png" alt="登录" style="zoom:50%;" />

**微信登录**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191621.png" alt="微信登录" style="zoom:50%;" />



### 后端管理页面

#### 登录和首页

**管理员登录**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191807.png" alt="后端登录" style="zoom:50%;" />

**后台首页**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191811.png" alt="后台首页" style="zoom:50%;" />

#### 讲师管理

**讲师列表**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191814.png" alt="讲师管理-讲师列表" style="zoom:50%;" />

**添加讲师**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191819.png" alt="讲师-添加讲师" style="zoom:50%;" />

**修改讲师**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191823.png" alt="讲师-修改" style="zoom:50%;" />

#### 权限管理

**用户列表**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191756.png" alt="权限管理-用户列表" style="zoom:50%;" />

**为用户分配角色**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191748.png" alt="权限管理-为用户分配角色" style="zoom:50%;" />

**角色列表**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191728.png" alt="权限管理-角色列表" style="zoom:50%;" />

**添加角色**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191733.png" alt="权限管理-添加角色" style="zoom:50%;" />

**为角色分配权限**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191744.png" alt="权限管理-为角色分配权限" style="zoom:50%;" />

**功能菜单列表**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191725.png" alt="权限管理-菜单列表" style="zoom:50%;" />

**添加功能**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191752.png" alt="权限管理-修改权限路由" style="zoom:50%;" />



#### 课程管理

**课程列表**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191708.png" alt="课程管理-课程列表" style="zoom: 50%;" />

**课程分类列表**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930193531.jpg" alt="课程分类列表" style="zoom:50%;" />

**导入课程分类**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191700.png" alt="课程-导入课程分类" style="zoom:50%;" />

**创建新的课程**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191713.png" alt="课程-填写课程基本信息" style="zoom:50%;" />

**编写课程大纲**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191656.png" alt="课程-创建课程大纲" style="zoom:50%;" />

**添加新的章节**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930194054.jpg" alt="章节" style="zoom: 50%;" />

**添加新的小节**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930194052.jpg" alt="小节" style="zoom: 50%;" />

**最终发布课程**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191703.png" alt="课程-发布课程" style="zoom:50%;" />



#### 统计分析

**生成统计数据**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191801.png" alt="统计-生成统计" style="zoom:50%;" />

**筛选统计数据**

<img src="https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20210930191804.png" alt="统计-选择统计日期" style="zoom:50%;" />



## 架构设计

- **架构设计需要考虑的几个方面：**

  - **性能：主要考虑访问频率，每个用户每天的访问次数。项目初始阶段用户的访问量并不大，如果考虑做运营推广，可能会迎来服务器访问量骤增，因此要考虑分布式部署，引入缓存**
  - **可扩展性：系统功能会随着用户量的增加以及多变的互联网用户需求不断地扩展，因此考虑到系统的可扩展性的要求需要使用微服务架构，引入消息中间件**
  - **高可用：** 系统一旦宕机，将会带来不可挽回的损失，因此必须做负载均衡，甚至是异地多活这类复杂的方案。如果数据丢失，修复将会非常麻烦，只能靠人工逐条修复，这个很难接受，因此需要考虑存储高可靠。我们需要考虑多种异常情况：机器故障、机房故障，针对机器故障，我们需要设计 MySQL 同机房主备方案；针对机房故障，我们需要设计 MySQL 跨机房同步方案。
  - **安全性：** 系统的信息有一定的隐私性，例如用户的个人身份信息，不包含强隐私（例如玉照、情感）的信息，因此使用账号密码管理、数据库访问权限控制即可。
  - **成本：** 视频类网站的主要成本在于服务器成本、流量成本、存储成本、流媒体研发成本，中小型公司可以考虑使用云服务器和云服务。

  

  ![img](https://gitee.com/geng_kun_yuan/myimg/raw/master/img/20211001184318.png)

  

## 部署流程

<font color=DarkMagenta size=4px>**后端部署流程**</font>

1. 下载项目，解压到本地路径，使用IDEA或其他工具打开项目
2. 把项目资料/数据库脚本目录下的数据库文件导入本地数据库
3. 修改所有application.properties文件中的数据源，使用自己本地的库名，用户名和密码
4. 修改相关路径，配置正确的图片路径、文件路径、附件路径、Mapper扫描路径。
5. 修改所有application.properties文件中的Nacos服务，使用自己的Nacos服务地址和端口
6. 修改所有application.properties文件中的Redis服务，使用自己的Redis服务地址和端口
7. 注意pom.xml中的依赖导入，确保所有的依赖在本地正确安装
8. 尤其需要注意阿里云视频上传的相关依赖，由于该依赖没有开源，无法从中央仓库下载，需要自己手动添加到自己的项目中
9. 修改MyGuliClass/service/service-oss模块下application.properties文件中有关阿里云OSS的配置，使用自己的keyid，keysecret，bucketname和endpoint
10. 修改MyGuliClass/service/service-msm模块下application.properties文件中有关阿里云短信的配置，使用自己的accesskeyid，secret，regionid
11. 修改MyGuliClass/service/service-vod模块下application.properties文件中有关视频点播的配置，使用自己的keyId，keysecret
12. 修改MyGuliClass/service/service-ucenter模块下application.properties文件中有关视频点播的配置，使用自己的app_id，app_secret，redirect_url
13. 启动所有的XXXApplication.java中的main方法，控制台没有报错信息，显示服务启动的端口号和启动时间即运行成功



<font color=DarkMagenta size=4px>**前端部署流程：后台管理部分**</font>

1. 下载项目，解压到本地路径，使用VScode或其他工具打开项目
2. 进入到MyGuliClassVue\vue-admin-1010目录下，执行 npm install 命令，安装项目所有需要的依赖
3. 执行 npm run dev 命令，启动后台管理项目
4. 控制台显示 Your application is running here: http://localhost:9528 ，并会自动打开浏览器
5. 控制台没有报错信息，表示项目启动成功
6. 在浏览器中输入正确的用户名和密码即可正常使用



<font color=DarkMagenta size=4px>**前端部署流程：前台展示部分**</font>

1. 下载项目，解压到本地路径，使用VScode或其他工具打开项目
2. 进入到MyGuliClassVue\vue-front-1010目录下，执行 npm install 命令，安装项目所有需要的依赖
3. 执行 npm run dev 命令，启动前端展示项目
4. 控制台显示服务的地址和内存占用情况
5. 控制台没有报错信息，表示项目启动成功
6. 在浏览器中输入 http://localhost:3000/，即可正常访问

