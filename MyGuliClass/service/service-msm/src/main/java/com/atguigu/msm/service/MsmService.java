package com.atguigu.msm.service;

import java.util.Map;

/**
 * @Author: GengKY
 * @Date: 2021/9/22 19:25
 */
public interface MsmService {
    public boolean send(Map<String, Object> param, String phone);
}
