package com.atguigu.serviceedu.service.impl;

import com.atguigu.serviceedu.entity.EduCourseDescription;
import com.atguigu.serviceedu.mapper.EduCourseDescriptionMapper;
import com.atguigu.serviceedu.service.EduCourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author gengky
 * @since 2021-09-20
 */
@Service
public class EduCourseDescriptionServiceImpl extends ServiceImpl<EduCourseDescriptionMapper, EduCourseDescription> implements EduCourseDescriptionService {

}
