package com.atguigu.serviceedu.entity.subject;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: GengKY
 * @Date: 2021/9/20 9:54
 */

@Data
public class OneSubject {
    private String id;
    private String title;
    //一个一级分类有多个二级分类
    private List<TwoSubject> children=new ArrayList<>();
}
