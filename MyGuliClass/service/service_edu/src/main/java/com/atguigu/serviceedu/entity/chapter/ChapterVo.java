package com.atguigu.serviceedu.entity.chapter;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: GengKY
 * @Date: 2021/9/20 16:54
 *
 * 表示章节
 */
@Data
public class ChapterVo {

    private String id;
    private String title;

    List<VideoVo> children=new ArrayList<>();


}
