package com.atguigu.serviceedu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 课程简介 前端控制器
 * </p>
 *
 * @author gengky
 * @since 2021-09-20
 */
@RestController
@RequestMapping("/eduServiceedu/edu-course-description")
public class EduCourseDescriptionController {

}

