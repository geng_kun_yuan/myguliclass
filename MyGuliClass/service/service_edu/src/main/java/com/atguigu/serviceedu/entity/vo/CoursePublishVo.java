package com.atguigu.serviceedu.entity.vo;

import lombok.Data;

/**
 * @Author: GengKY
 * @Date: 2021/9/21 10:54
 *
 *  课程发布最终确认的封装类
 */
@Data
public class CoursePublishVo {
    private String id;
    private String title;
    private String cover;
    private Integer lessonNum;
    private String subjectLevelOne;
    private String subjectLevelTwo;
    private String teacherName;
    private String price;//只用于显示

}
