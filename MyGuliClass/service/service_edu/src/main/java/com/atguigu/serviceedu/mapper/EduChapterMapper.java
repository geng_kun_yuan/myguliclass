package com.atguigu.serviceedu.mapper;

import com.atguigu.serviceedu.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author gengky
 * @since 2021-09-20
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
