package com.atguigu.serviceedu.entity.chapter;

import lombok.Data;

/**
 * @Author: GengKY
 * @Date: 2021/9/20 16:54
 *
 * 表示小节
 */
@Data
public class VideoVo {
    private String id;
    private String title;

    private String videoSourceId;
}
