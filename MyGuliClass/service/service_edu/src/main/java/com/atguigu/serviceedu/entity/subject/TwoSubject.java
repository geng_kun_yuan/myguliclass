package com.atguigu.serviceedu.entity.subject;

import lombok.Data;

/**
 * @Author: GengKY
 * @Date: 2021/9/20 9:54
 */
@Data
public class TwoSubject {
    private String id;
    private String title;
}
