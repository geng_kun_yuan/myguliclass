package com.atguigu.eduorder.mapper;

import com.atguigu.eduorder.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author gengky
 * @since 2021-09-24
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
