import request from '@/utils/request'

export default{

    /**
     * 获取所有的课程分类，树形
     * @returns 
     */
    getSubjectList(){
        return request({
            url: `/eduService/subject/getAllSubject`,
            method: 'get'
          })
    }

    
}