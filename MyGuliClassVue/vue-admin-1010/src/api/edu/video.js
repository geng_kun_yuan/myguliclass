import request from '@/utils/request'
export default {

    /**
     * 添加小节
     * @param {*} video 
     * @returns 
     */
    addVideo(video) {
        return request({
            url: '/eduService/video/addVideo',
            method: 'post',
            data: video
          })
    },
    
    /**
     * 删除小节
     * @param {*} id 
     * @returns 
     */
     deleteVideo(id) {
        return request({
            url: '/eduService/video/deleteVideoById/'+id,
            method: 'delete'
          })
    },

    /**
     * 更新小节
     * @param {*} id 
     * @returns 
     */
    updateVideo(video) {
        return request({
            url: '/eduService/video/updateVideo',
            method: 'post',
            data: video
          })
    },

    /**
     * 根据ID获取小节信息
     * @param {*} videoId 
     */
     getVideo(videoId){
        return request({
            url: `/eduService/video/getVideoInfoById/${videoId}`,
            method: 'get'
          })
    },

    /**
     * 根据ID删除云端视频
     * @param {*} videoId 
     * @returns 
     */
    deleteAliyunvod(videoId){
      return request({
          url: `/eduvod/video/removeAlyVideo/${videoId}`,
          method: 'delete'
        })
    },




}