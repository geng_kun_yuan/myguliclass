import request from '@/utils/request'

export default{

    /**
     * 讲师列表（条件查询带分页）
     * @param {*} current 当前页
     * @param {*} limit 每页记录数
     * @param {*} teacherQuery 条件对象
     * @returns 
     */
    getTeacherListPage(current,limit,teacherQuery){
        return request({
            //url: '/serviceEdu/teacher/PageCondition/'+current+'/'+limit,
            url: `/eduService/teacher/PageCondition/${current}/${limit}`,
            method: 'post',
            //teacherQuery条件对象，后端使用 RequestBody获取数据
            //data表示把对象转换JSON进行传递到接口里面
            data: teacherQuery
          })
    },

    /**
     * 删除讲师
     * @param {*} id 
     * @returns 
     */
    deleteTeacherId(id) {
        return request({
            url: `/eduService/teacher/removeById/${id}`,
            method: 'delete'
          })
    },
    /**
     * 添加讲师
     * @param {*} teacher 
     * @returns 
     */
    addTeacher(teacher) {
        return request({
            url: `/eduService/teacher/addTeacher`,
            method: 'post',
            data: teacher
          })
    },
    /**
     * 根据id查询讲师
     * @param {*} id 
     * @returns 
     */
    getTeacherInfo(id) {
        return request({
            url: `/eduService/teacher/selectById/${id}`,
            method: 'get'
          })
    },
    /**
     * 修改讲师
     * @param {*} teacher 
     * @returns 
     */
    updateTeacherInfo(teacher) {
        return request({
            url: `/eduService/teacher/saveOrUpdateTeacher`,
            method: 'post',
            data: teacher
          })
    }



}

export function login(username, password) {
  
}