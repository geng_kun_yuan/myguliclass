import request from '@/utils/request'
export default {
    /**
     * 1 添加课程信息
     * @param {*} courseInfo 
     * @returns 
     */
    addCourseInfo(courseInfo) {
        return request({
            url: '/eduService/course/addCourseInfo',
            method: 'post',
            data:courseInfo
          })
    },
    /**
     * 查询所有讲师
     * @returns 
     */
    getListTeacher() {
        return request({
            url: '/eduService/teacher/findAll',
            method: 'get'
          })
    },
    /**
     * 根据课程id查询课程基本信息
     * @param {*} id 
     * @returns 
     */
    getCourseInfoId(courseId) {
        return request({
            url: `/eduService/course/getCourseInfoById/${courseId}`,
            method: 'get'
          })
    },
    /**
     * 修改课程信息
     * @param {*} courseInfo 
     * @returns 
     */
    updateCourseInfo(courseInfo) {
        return request({
            url: '/eduService/course/updateCourseInfo',
            method: 'post',
            data: courseInfo
          })
    },

    /**
     * 课程信息最终确认
     * @param {*} courseInfo 
     * @returns 
     */
     getPublihCourseInfo(courseId) {
        return request({
            url: '/eduService/course/getPublishCourseInfo/'+courseId,
            method: 'get',
          })
    },

    /**
     * 最终发布课程
     * @param {*} courseId 
     * @returns 
     */
    publihCourse(courseId) {
        return request({
            url: '/eduService/course/publishCourse/'+courseId,
            method: 'post',
          })
    },


    /**
     * 获取课程列表信息
     * @returns 
     */
     getListCourse() {
        return request({
            url: '/eduService/course/getCourseList',
            method: 'get',
          })
    },


    /**
     * 删除课程
     */
     deleteCourseId(courseid){
       console.log("删除课程的请求")
      return request({
        url: `/eduService/course/deleteCourseById/${courseid}`,
        method: 'delete'
      })
     }

    //TODO:获取课程列表信息


}
