import request from '@/utils/request'

export default {
  //登录的方法
  getTeacherList(page,limit) {
    return request({
      url: `/eduService/teacherfront/getTeacherFrontList/${page}/${limit}`,
      method: 'post'
    })
  },

  //根据讲师ID查询讲师的基本信息,所讲课程的基本信息
  getTeacherInfo(teacherid){
    return request({
        url: `/eduService/teacherfront/getTeacherFrontInfo/${teacherid}`,
        method: 'get'
      })
  }
}