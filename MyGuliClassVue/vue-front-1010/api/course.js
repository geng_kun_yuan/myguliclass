import request from '@/utils/request'
export default {

    /**
     * 条件分页查询课程的方法
     * @param {*} page 
     * @param {*} limit 
     * @param {*} searchObj 
     * @returns 
     */
     getCourseList(page,limit,searchObj) {
    return request({
      url: `/eduService/coursefront/getFrontCourseList/${page}/${limit}`,
      method: 'post',
      data: searchObj
    })
  },

  /**
   * 获取所有的课程 一级、二级分类
   * @returns 
   */
  getAllSubject(){
    return request({
        url: `/eduService/subject/getAllSubject`,
        method: 'get'
      })
  },

  /**
   * 获取课程的所有相关信息
   * @returns 
   */
   getCourseInfo(courseId){
    return request({
        url: `/eduService/coursefront/getFrontCourseInfo/${courseId}`,
        method: 'get'
      })
  },


}