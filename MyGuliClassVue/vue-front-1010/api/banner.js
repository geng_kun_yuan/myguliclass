import request from '@/utils/request'
export default {

  /**
   * 获取轮播图
   * @returns 
   */  
   getListBanner() {
    return request({
      url: `/educms/bannerfront/getAllBanner`,
      method: 'get'
    })
  }
}